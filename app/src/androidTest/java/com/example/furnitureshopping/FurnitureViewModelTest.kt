package com.example.furnitureshopping

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.platform.app.InstrumentationRegistry
import com.example.furnitureshopping.UI.Furnitures.furniture
import com.example.furnitureshopping.ViewModels.FurnitureViewModel

import com.example.furnitureshopping.dao.FurnitureDao
import com.example.furnitureshopping.data.Furniture
import com.example.furnitureshopping.db.FurnitureDatabase
import com.example.furnitureshopping.repository.FurnitureApiService
import com.example.furnitureshopping.repository.FurnitureRepository
import org.hamcrest.CoreMatchers.equalTo
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ProductListViewModelTest {
    private lateinit var appDatabase:FurnitureDatabase
    private lateinit var viewModel: FurnitureViewModel
    private lateinit var furnitureDao: FurnitureDao

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        appDatabase = Room.inMemoryDatabaseBuilder(context, FurnitureDatabase::class.java).build()
        furnitureDao = appDatabase.furnitureDao()

        }

        //val productRepo = FurnitureRepository(FurnitureDatabase, FurnitureApiService.getInstance())
      //  FurnitureViewModel= FurnitureViewModel(FurnitureRepository)
    }


    @Test
    @Throws(InterruptedException::class)
    fun testDefaultValues() {
        //assertThat(FurnitureViewModel.addFurniture.value?.size,equalTo(3))
    }
