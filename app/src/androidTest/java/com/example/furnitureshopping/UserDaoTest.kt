package com.example.furnitureshopping

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.platform.app.InstrumentationRegistry
import com.example.furnitureshopping.dao.UserDao
import com.example.furnitureshopping.db.FurnitureDatabase

import org.hamcrest.CoreMatchers.equalTo
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class UserDaoTest {
    private lateinit var database:FurnitureDatabase
    private lateinit var userDao: UserDao
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDatabase(){
        val context  = InstrumentationRegistry.getInstrumentation().targetContext
        database = Room.inMemoryDatabaseBuilder(context,FurnitureDatabase::class.java).build()
        userDao = database.userDao()

    }
    @After
    fun closeDatabase(){
        database.close()
    }
    @Test
    fun testInsertuser(){
      //  userDao.insertUser(testUser[0])
        assertThat(userDao.getAllUsers().value?.size,equalTo(2))
    }


}