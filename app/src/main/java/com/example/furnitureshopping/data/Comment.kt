package com.example.furnitureshopping.data

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import com.example.furnitureshopping.data.Furniture
import java.io.Serializable
import java.util.*

@Entity(foreignKeys = [ForeignKey(entity = Furniture::class, parentColumns = ["id"], childColumns = ["productId"], onDelete = ForeignKey.CASCADE)
    ]
    , indices = [Index(value = ["productId"])])
data class Comment(
    @PrimaryKey(autoGenerate = true) val id:Long,
    val productId:Long,
    val content:String,
    val username:String,
    val createdAt:String = Date().toString()


): Serializable