package com.example.furnitureshopping.data

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity
data class Furniture(
    val description: String,
    @PrimaryKey(autoGenerate = true) val id: Long,
    val title: String,
    val users: List<User>,
    val price: String
)
