package com.example.furnitureshopping.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey

@Entity(
    tableName = "registered_users",
    foreignKeys = [ForeignKey(entity = Furniture::class, parentColumns = ["id"], childColumns = ["furniture_id"] , onDelete = ForeignKey.RESTRICT), ForeignKey(entity = User::class, parentColumns = ["id"], childColumns = ["user_id"], onDelete = ForeignKey.RESTRICT)]
)
data class RegisteredUsers(
    @ColumnInfo(name = "furniture_id") val furniture_id: Long,
    @ColumnInfo(name = "user_id") val user_id : Long
)
