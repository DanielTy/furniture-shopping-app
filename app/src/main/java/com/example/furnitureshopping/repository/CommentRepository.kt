package com.example.furnitureshopping.repository

import androidx.lifecycle.LiveData
import com.example.furnitureshopping.dao.CommentDao

import com.example.furnitureshopping.data.Comment
import javax.inject.Singleton

@Singleton
class CommentRepository constructor(private val commentDao: CommentDao) {

    fun allCommentsbyProductId(productId:Long): LiveData<List<Comment>> = commentDao.getAllCommentsByProductId(productId)
    fun insertComment(comment: Comment) = commentDao.insertComment(comment)
    fun updateComment(comment: Comment) = commentDao.updateComment(comment)
    fun deleteComment(comment: Comment) = commentDao.deleteComment(comment)
}


