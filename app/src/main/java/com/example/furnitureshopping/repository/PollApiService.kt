package com.example.furnitureshopping.repository

import com.example.furnitureshopping.data.Poll
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

interface PollApiService {

    @GET("/poll/{id}")
    fun getAPoll(@Path("id") id: Long) : Deferred<Response<Poll>>

    @PATCH("/poll/vote/{id}")
    fun vote( @Body answer : String) : Deferred<Response<Poll>>

    @PATCH("/poll/close/{id}")
    fun close( ) : Deferred<Response<String>>

    @POST("/poll")
    fun createPoll(@Body poll: Poll) : Deferred<Response<Poll>>

}