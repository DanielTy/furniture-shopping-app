package com.example.furnitureshopping.repository

import androidx.lifecycle.LiveData
import com.example.furnitureshopping.dao.FurnitureDao
import com.example.furnitureshopping.data.Furniture

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

import retrofit2.Response
class FurnitureRepository(private val furnitureApiService: FurnitureApiService, private  val furnitureDao: FurnitureDao) {

    fun AllFurnitures():LiveData<List<Furniture>>{
        return furnitureDao.getAllFurnitures()
    }

    fun AFurniture(id:Long):LiveData<Furniture>{
        return furnitureDao.furnitureById(id)

    }

    fun insertFurniture(furniture: Furniture):Long{
        furnitureDao.insertFurniture(furniture)
        return 1
    }




    suspend fun getAllFurnitures(): Response<List<Furniture>> =
        withContext(Dispatchers.IO){
            furnitureApiService.getAllFurnitures().await()
        }

    suspend fun getAFurnitures(id:Long): Response<Furniture> =
        withContext(Dispatchers.IO){
            furnitureApiService.getAFurnitures(id).await()
    }

    suspend fun addFurniture(furniture:Furniture, token:String): Response<Furniture> =
        withContext(Dispatchers.IO){
            furnitureApiService.addFurniture(furniture,token).await()
        }

    suspend fun registerForFurniture(user_id:Long, furniture_id:Long, token:String): Response<Furniture> =
        withContext(Dispatchers.IO){
            furnitureApiService.registerForFurniture(user_id,furniture_id,token).await()
        }

    suspend fun updateFurniture(id:Long, token:String):Response<Furniture> =
        withContext(Dispatchers.IO){
            furnitureApiService.updateFurnitures(id,token).await()
        }








}