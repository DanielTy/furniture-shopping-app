package com.example.furnitureshopping.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.furnitureshopping.dao.FurnitureDao
import com.example.furnitureshopping.dao.UserDao
import com.example.furnitureshopping.data.Furniture

@Database(entities = arrayOf(Furniture::class),version = 1)
abstract class FurnitureDatabase :RoomDatabase(){
    abstract fun furnitureDao():FurnitureDao
    abstract fun userDao(): UserDao;

    companion object {

        @Volatile
        private var INSTANCE: FurnitureDatabase? = null

        fun getDatabase(context: Context): FurnitureDatabase {

            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {

                val instance = Room.databaseBuilder(context.applicationContext, FurnitureDatabase::class.java, "furnitureshopping_database"
                ).build()

                INSTANCE = instance
                return instance
            }
        }
    }






}