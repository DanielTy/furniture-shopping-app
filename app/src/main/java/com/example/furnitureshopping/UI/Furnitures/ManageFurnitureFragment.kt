package com.example.furnitureshopping.UI.Furnitures

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.furnitureshopping.ViewModels.FurnitureViewModel
import com.example.furnitureshopping.data.Furniture
import kotlinx.android.synthetic.main.fragment_manage_content.view.*

class ManageFurnitureFragment : Fragment() {

    private lateinit var furnitureViewModel: FurnitureViewModel

    private lateinit var idEditText: EditText
    private lateinit var descriptionEditText: EditText
    private lateinit var priceEditText: EditText



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        furnitureViewModel = ViewModelProviders.of(this).get(FurnitureViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(com.example.furnitureshopping.R.layout.fragment_manage_content, container, false)

        idEditText = view.content_one_text
        priceEditText = view.content_two_text





       /* updateButton.setOnClickListener {
            listener.onUpdateButtonClicked(readFields())
            clearFields()
        }

        deleteButton.setOnClickListener {
            listener.onDeleteButtonClicked(readFields())
            clearFields()
        }*/




        return view
    }

    private fun updateFields(furniture: Furniture){
        furniture.run{
            priceEditText.setText(furniture.price)
            descriptionEditText.setText(furniture.description)
        }
    }


    private fun readFields() {
        idEditText.text.toString()
        priceEditText.text.toString()
        descriptionEditText.text.toString()
    }

    private fun clearFields() {
        idEditText.setText("")
        priceEditText.setText("")
    }

}
