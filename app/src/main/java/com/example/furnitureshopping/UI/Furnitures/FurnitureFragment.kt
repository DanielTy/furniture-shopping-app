package com.example.furnitureshopping.UI.Furnitures

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_main.list_recycler_view
import kotlinx.android.synthetic.main.furniture_fragment.fab

data class furniture(val title: String, val price: String, val image: String)
class FurnitureFragment : Fragment(){







    private val nicCagefurnitures = listOf(
        furniture("home made", "200 birr", "raising_arizona.jpg"),
        furniture("classic", "200 birr", "vampires_kiss.png"),
        furniture("art pil", "200 birr", "con_air.jpg"),
        furniture("wooden chier", "200 birr", "face_off.jpg"),
        furniture("table", "200 birr", "national_treasure.jpg"),
        furniture("bad", "200 birr", "wicker_man.jpg"),
        furniture("shelf", "200 birr", "bad_lieutenant.jpg"),
        furniture("wooden table", "200 birr", "kickass.jpg")
    )


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true


    }



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(com.example.furnitureshopping.R.layout.furniture_fragment, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        list_recycler_view.apply {
            layoutManager = LinearLayoutManager(this@FurnitureFragment.activity)
            adapter = ListAdapter(nicCagefurnitures)



        }

        fab.setOnClickListener{
            val fr = fragmentManager!!.beginTransaction()
            fr.replace(
                com.example.furnitureshopping.R.id.relativeLayout,
                ManageFurnitureFragment()
            )
            fr.addToBackStack(null)
            fr.commit()
        }
    }

    private fun loadManagerFurnitureFragment(falg: ManageFurnitureFragment) {
        ManageFurnitureFragment()

    }

    companion object {
        fun newInstance(): FurnitureFragment =
            FurnitureFragment()
    }}