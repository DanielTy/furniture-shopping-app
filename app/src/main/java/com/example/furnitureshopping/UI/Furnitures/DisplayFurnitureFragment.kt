package com.example.furnitureshopping.UI.Furnitures

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.furnitureshopping.R
import com.example.furnitureshopping.ViewModels.FurnitureViewModel
import kotlinx.android.synthetic.main.fragment_display_content.view.*

class DisplayFurnitureFragment : Fragment() {

    private lateinit var furnitureViewModel: FurnitureViewModel

    private lateinit var furnitureIdTextView: TextView
    private lateinit var furniturePriceTextView: TextView
    private lateinit var furnitureDescriptionTextView: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        furnitureViewModel = ViewModelProviders.of(this).get(FurnitureViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_display_content, container, false)

        furnitureIdTextView = view.display_one_text
        furniturePriceTextView = view.display_two_text
        furnitureDescriptionTextView = view.display_text_three

        val id = arguments?.getString("furniture_id") as Long

        val liveFurniture = furnitureViewModel.getAFurnitures(id)


        return view
    }

    companion object {

        fun newInstance(furnitureID: String): DisplayFurnitureFragment {
            val displayContentFragment = DisplayFurnitureFragment()

            val args = Bundle()
            args.putString("furniture_title", furnitureID)

            displayContentFragment.arguments = args

            return displayContentFragment
        }
    }

}
