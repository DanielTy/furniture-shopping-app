package com.example.furnitureshopping.UI.Furnitures

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ListAdapter(private val list: List<furniture>)
    : RecyclerView.Adapter<MovieViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return MovieViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val furniture: furniture = list[position]
        holder.bind(furniture)
    }

    override fun getItemCount(): Int = list.size

}

class MovieViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(com.example.furnitureshopping.R.layout.list_item, parent, false)) {
    private var mTitleView: TextView? = null
    private var mYearView: TextView? = null


    init {
        mTitleView = itemView.findViewById(com.example.furnitureshopping.R.id.list_title)
        mYearView = itemView.findViewById(com.example.furnitureshopping.R.id.list_description)
    }

    fun bind(furniture: furniture) {
        mTitleView?.text = furniture.title
        mYearView?.text = furniture.price
    }

}