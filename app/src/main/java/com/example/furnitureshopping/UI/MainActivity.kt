package com.example.furnitureshopping.UI

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import kotlinx.android.synthetic.main.activity_main.*


//import et.edu.aait.listdetailfragmentappliction.ManageFurnitureFragment

class MainActivity : AppCompatActivity(){
    private lateinit var navController: NavController


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.example.furnitureshopping.R.layout.activity_main)
        setSupportActionBar(toolBar)
        navController = Navigation.findNavController(this, com.example.furnitureshopping.R.id.nav_host_fragment)
        bottom_nav.setupWithNavController(navController)
        NavigationUI.setupActionBarWithNavController(this,navController)
//        navController.navigate(R.id.settingFragment)

    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp()
    }
}
