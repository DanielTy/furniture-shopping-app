package com.example.furnitureshopping.UI.Furnitures

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.furnitureshopping.viewmodel.loginViewModel


class loginFragment: Fragment() {
    companion object {
        fun newInstance() = loginFragment()
    }

    private lateinit var viewModel: loginViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(com.example.furnitureshopping.R.layout.login_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(loginViewModel::class.java)
        // TODO: Use the ViewModel
    }

}