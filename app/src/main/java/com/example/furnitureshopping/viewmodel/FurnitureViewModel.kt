package com.example.furnitureshopping.ViewModels

import android.app.Application
import androidx.lifecycle.*
import com.example.furnitureshopping.data.Furniture
import com.example.furnitureshopping.db.FurnitureDatabase
import com.example.furnitureshopping.repository.FurnitureApiService
import com.example.furnitureshopping.repository.FurnitureRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response

class FurnitureViewModel(application:Application):AndroidViewModel(application) {
    val furnituregRepository: FurnitureRepository

    init {
        val furnitureApiService = FurnitureApiService.getInstance()

        val furnitureDao = FurnitureDatabase.getDatabase(application).furnitureDao()
        furnituregRepository = FurnitureRepository(furnitureApiService,furnitureDao)
    }
    private  val _getAllFurniture = MutableLiveData<Response<List<Furniture>>>()
    val getAllFurniture: LiveData<Response<List<Furniture>>>
        get() = _getAllFurniture

    private val _getAFurniture = MutableLiveData<Response<Furniture>>()
    val getAFurniture: LiveData<Response<Furniture>>
        get() = _getAFurniture

    private val _addFurniture = MutableLiveData<Response<Furniture>>()
    val addFurniture: LiveData<Response<Furniture>>
        get() = _addFurniture

    private val _registerForFurniture = MutableLiveData<Response<Furniture>>()
    val registerForFurniture: LiveData<Response<Furniture>>
        get() = _registerForFurniture


    private val _updateFurniture = MutableLiveData<Response<Furniture>>()
    val updateFurniture: LiveData<Response<Furniture>>
        get() = _updateFurniture

    fun getAllFurnitures() = viewModelScope.launch{
        _getAllFurniture.postValue(furnituregRepository.getAllFurnitures())


    }
 
    fun getAFurnitures(id:Long) = viewModelScope.launch {

        _getAFurniture.postValue(furnituregRepository.getAFurnitures(id))
    }

    fun addFurnitures(furniture:Furniture,token:String) = viewModelScope.launch {
        _addFurniture.postValue(furnituregRepository.addFurniture(furniture,token))
    }

    fun registerForFurnitures(user_id:Long,furniture_id:Long,token:String) = viewModelScope.launch {
        _registerForFurniture.postValue(furnituregRepository.registerForFurniture(user_id,furniture_id,token))
    }

    fun updateFurnitures(id:Long,token:String) = viewModelScope.launch {
        _updateFurniture.postValue(furnituregRepository.updateFurniture(id,token))
    }




    suspend fun AllFurniture():LiveData<List<Furniture>> =

        withContext(Dispatchers.IO){
            furnituregRepository.AllFurnitures()
        }

    suspend fun AFurniture(id:Long):LiveData<Furniture> =
        withContext(Dispatchers.IO){
            furnituregRepository.AFurniture(id)
        }

    suspend fun insert(furniture: Furniture):Long =
        withContext(Dispatchers.IO){
            furnituregRepository.insertFurniture(furniture)
        }









}