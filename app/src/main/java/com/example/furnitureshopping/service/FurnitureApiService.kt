package com.example.furnitureshopping.service


import com.example.furnitureshopping.data.Furniture
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

interface FurnitureApiService {

    @GET("/furniture")
    fun getAllFurnitures(): Deferred<Response<List<Furniture>>>

    @GET("/furniture/{id}")
    fun getAFurniture(@Path("id") id:Long) : Deferred<Response<Furniture>>

    @POST("/furniture?token={token}")
    fun addFurniture(@Body furniture: Furniture ,@Query("token") token:String) : Deferred<Response<Furniture>>

//    @DELETE("/furniture/{id}?token={token}")
//    fun deletecom.example.furnitureshopping.data.Furniture(@Path("id") id: Long ,@Query("token") token:String) : Deferred<Response<com.example.furnitureshopping.data.Furniture>>

    @POST("/furniture/registration?token={token}")
    fun registerForFurniture(@Body user_id: Long, @Body furniture_id: Long ,@Query("token") token:String) : Deferred<Response<Furniture>>

    @DELETE("/furniture/registration/{id}?token={token}")
    fun unregisterFromFurniture(@Path("id") id: Long ,@Query("token") token:String) : Deferred<Response<Furniture>>

    @PUT("/furniture/{id?token={token}}")
    fun updateFurniture(@Path("id") id: Long ,@Query("token") token:String): Deferred<Response<Furniture>>

    abstract fun furnitureApiService() : FurnitureApiService
    abstract fun userApiService() : UserApiService

    companion object{
        private val baseUrl = "http://localhost:8080/api/v1/"
        fun getInstance(): FurnitureApiService {
            val retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()
            return retrofit.create(FurnitureApiService::class.java)
        }
    }

}