package com.example.furnitureshopping.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.furnitureshopping.data.Furniture


@Dao
interface FurnitureDao {

    @Query("SELECT * from furnitures ORDER BY id")
    fun getAllFurnitures(): LiveData<List<Furniture>>

    @Query("SELECT * from furnitures where id = :id")
    fun furnitureById(id:Long):LiveData<Furniture>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFurniture(furniture: Furniture):Long

    @Delete
    fun deleteFurniture(furniture: Furniture) : Int

    @Update
    fun updateFurniture(furniture: Furniture) : Int


}