package com.example.furnitureshopping.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.furnitureshopping.data.Poll



@Dao
interface PollDao {

    @Query("select * from polls where id = :id")
    fun getAPoll(id:Long) : LiveData<Poll>

   @Update
   fun updatePoll(poll: Poll) : Int

    @Update
    fun closePoll(poll: Poll) :Int

    @Insert
    fun createPoll(poll: Poll) : Long


}