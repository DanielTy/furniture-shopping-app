package com.example.furnitureshopping.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.furnitureshopping.data.Comment

@Dao
interface CommentDao {
    @Query("SELECT * from comment WHERE productId= :productId ORDER BY createdAt")
    fun getAllCommentsByProductId(productId:Long): LiveData<List<Comment>>
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertComment(comment : Comment):Long
    @Update
    fun updateComment(comment : Comment):Int
    @Delete
    fun deleteComment(comment: Comment):Int


}